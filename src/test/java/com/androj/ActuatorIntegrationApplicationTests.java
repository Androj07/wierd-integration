package com.androj;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import io.restassured.RestAssured;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ActuatorIntegrationApplicationTests {


	@Value("${server.port}")
	Integer serverPost;

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(9090);

	@Before
	public void prepare(){
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void shouldReturnAllUp(){
		stubFor(get("/first")
				.willReturn(aResponse().withStatus(200)));
		stubFor(get("/second")
				.willReturn(aResponse().withStatus(200)));


		RestAssured.port = serverPost;
		System.out.println(WireMock.listAllStubMappings().getMappings());
		RestAssured.given()
				.get("/health")
				.then()
				.statusCode(200)
				.body("status",equalTo("UP"))
				.body("first.status",equalTo("UP"))
				.body("second.status",equalTo("UP"));
	}

	@Test
	public void shouldReturnAllDown(){

		stubFor(get("/first")
				.willReturn(aResponse().withStatus(500)));
		stubFor(get("/second")
				.willReturn(aResponse().withStatus(500)));


		RestAssured.port = serverPost;
		System.out.println(WireMock.listAllStubMappings().getMappings());
		RestAssured.given()
				.get("/health")
				.then()
				.assertThat()
				.statusCode(503)
				.body("status",equalTo("DOWN"))
				.body("first.status",equalTo("DOWN"))
				.body("second.status",equalTo("DOWN"));

	}

	@Test
	public void shouldReturnFirstUpSecondDown(){

		stubFor(get("/first")
				.willReturn(aResponse().withStatus(200)));
		stubFor(get("/second")
				.willReturn(aResponse().withStatus(500)));


		RestAssured.port = serverPost;
		System.out.println(WireMock.listAllStubMappings().getMappings());
		RestAssured.given()
				.get("/health")
				.then()
				.statusCode(503)
				.body("status",equalTo("DOWN"))
				.body("first.status",equalTo("UP"))
				.body("second.status",equalTo("DOWN"));

	}

	@Test
	public void shouldReturnFirstDownSecondUp(){
		System.out.println(WireMock.listAllStubMappings().getMappings());
		stubFor(get("/first")
				.willReturn(aResponse().withStatus(500)));
		stubFor(get("/second")
				.willReturn(aResponse().withStatus(200)));
		System.out.println(WireMock.listAllStubMappings().getMappings());
		RestAssured.port = serverPost;

		RestAssured.given()
				.get("/health")
				.then()
				.statusCode(503)
				.body("status",equalTo("DOWN"))
				.body("first.status",equalTo("DOWN"))
				.body("second.status",equalTo("UP"));

	}

}
