package com.androj.configuration;

import com.androj.monitoring.HealthChecker;
import com.androj.monitoring.MonitoringEndpoints;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by andrzejhankus on 26/06/2017.
 */
@Configuration
public class ApplicationConfig {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean(name = "firstHealthIndicator")
    public HealthIndicator firstHealthIndicator(MonitoringEndpoints endpoints){
        return new HealthChecker(restTemplate(),endpoints.getFirstList());
    }

    @Bean(name = "secondHealthIndicator")
    public HealthIndicator secondHealthIndicator(MonitoringEndpoints endpoints){
        return new HealthChecker(restTemplate(),endpoints.getSecondList());
    }
}
