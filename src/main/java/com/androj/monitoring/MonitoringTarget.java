package com.androj.monitoring;

/**
 * Created by andrzejhankus on 26/06/2017.
 */
public class MonitoringTarget {
    private String name;
    private String url;

    public MonitoringTarget(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public MonitoringTarget() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
