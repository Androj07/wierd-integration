package com.androj.monitoring;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrzejhankus on 26/06/2017.
 */
@Component
@ConfigurationProperties(prefix = "health")
public class MonitoringEndpoints {
    List<MonitoringTarget> firstList = new ArrayList<>();
    List<MonitoringTarget> secondList = new ArrayList<>();

    public List<MonitoringTarget> getFirstList() {
        return firstList;
    }

    public List<MonitoringTarget> getSecondList() {
        return secondList;
    }
}
