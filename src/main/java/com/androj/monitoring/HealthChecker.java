package com.androj.monitoring;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by andrzejhankus on 26/06/2017.
 */
public class HealthChecker implements HealthIndicator {

    private RestTemplate rest;
    private List<MonitoringTarget> targets;

    public HealthChecker(RestTemplate rest, List<MonitoringTarget> targets){
        this.rest = rest;
        this.targets = targets;
    }

    @Override
    public Health health() {

        Health.Builder health = Health.up();
        targets.forEach( target->{
            try {
                ResponseEntity<Void> healthResponse = rest.getForEntity(target.getUrl(), Void.class);
                if (isUnhealthy(healthResponse)) {
                    health.down();
                    health.withDetail(target.getName(), "Unhealthy " + healthResponse.getStatusCode());
                }
            }catch (Exception ex){
                health.down();
                health.withDetail(target.getName(), "Unhealthy " + ex.getMessage());
            }
        });
        return health.build();
    }

    private boolean isUnhealthy( ResponseEntity<Void> healthResponse){
        HttpStatus statusCode = healthResponse.getStatusCode();
        return statusCode.is4xxClientError()||statusCode.is5xxServerError();
    }
}
