package com.androj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorIntegrationApplication.class, args);
	}
}
